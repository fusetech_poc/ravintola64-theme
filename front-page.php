<?php get_header();?>


<?php include(get_theme_file_path( './contacts-verification.php'))?>


<div class="page-wrapper">
    <div class="page-content">
        <div class="about-wrapper" id="about">
            <div class="about-content">
                <div class="about-title-container">  
                    <h1>WE ARE</h1>
                </div>
                <div class="about-logo"></div>
                <div class="about-text-container">
                    <span>Parta Games is a Finnish independent game studio making organic friendly neighborhood video games.
                    </span>
                </div>
                <div class="contact-button"><a href="#contact">CONTACT US</a></div>
            </div>
        </div>
        <div class="games-wrapper" id="games">
            <div class="games-title">
                <div class="title-container">
                    <h1>OUR GAMES</h1>
                </div>
            </div>
            <input type="checkbox" id="load-more-games">
            <div class="games-cards-container">
                <div class="card-container" style="background-image: url('<?php bloginfo('template_url')?>/assets/images/cards/rescue_rivals_card.png')">
                    <div class="card-header"><h3>CHOPPA: RESCUE RIVALS</h3></div>
                    <div class="card-body">
                        <div class="card-description">
                            <p>Choppa: Rescue Rivals is a chaotic arcade-style helicopter rescue competition game
                                for one to four players. Take off with your trusty rescue chopper and carry
                                disaster victims to safety before your rivals do! All bets are off in this high-flying,
                                physics-driven game spanning multiple levels from the mountains of
                                Nepal to an offshore oil tanker disaster. In Choppa: Rescue Rivals you compete in extreme 
                                rescue matches against casual or competitive online opponents and locally with friends.
                                Choppa: Rescue Rivals has tons of cosmetic customization options,
                                including helicopters, cool pilot characters, stickers, and accessories and rule mutators to spice up the game.
                            </p>
                        </div>
                    </div>
                    <div class="card-footer"> <a href="https://store.steampowered.com/app/1173100/Choppa_Rescue_Rivals/">STEAM</a></div>
                </div>
                <div class="card-container" style="background-image: url('<?php bloginfo('template_url')?>/assets/images/cards/choppa_card.png')">
                    <div class="card-header"><h3>CHOPPA</h3></div>
                    <div class="card-body">
                        <div class="card-description">
                            <p>Over 2 million downloads! You are Rick Guiver, a reckless coast guard rescue helicopter pilot.
                                A terrible oil rig catastrophe has happened in the coast!
                                Against your superiors' orders and better judgement you jump in your helicopter and fly to the danger zone!
                                There's no time to lose, save as many people as you can!
                                Choppa is a physics based arcade and action game with touch controls, 
                                procedurally generated stages and 1980's style rock music. The main draw of the game is progressing 
                                through randomly generated levels and saving as many people as possible at the risk of exploding or falling to the sea.
                                Players will be able to purchase new helicopters, upgrade armor, 
                                engines and rescue gear and complete challenging missions while aiming for the top score.
                                Game Center and Google Play Games leaderboards are supported.
                            </p>
                        </div>
                    </div>
                    <div class="card-footer"> <a href="https://choppagame.com/">WEBSITE</a></div>
                </div>
                <div class="card-container" style="background-image: url('<?php bloginfo('template_url')?>/assets/images/cards/noodle_road_card.png')">
                    <div class="card-header"><h3>NOODLE ROAD</h3></div>
                    <div class="card-body">
                        <div class="card-description" >
                            <p>Roll the ball along a swirly noodle! Avoid obstacles and aim for the glassy zones!
                                Hit perfect combos and score even more in fever mode!
                                Speed through literally twisted levels or try the challenging endless mode and reach the high score!
                                Good luck... but don't lose your noodle!
                                Lots of noodles were eaten during development of this game.
                            </p>
                        </div>
                    </div>
                    <div class="card-footer"><a href="https://apps.apple.com/app/id1449804643">APPSTORE
                </a></div>
                </div>
                <!-- Extra Cards -->
                <!-- 
                <div class="card-container">
                    <div class="card-header"></div>
                    <div class="card-body"></div>
                    <div class="card-footer"></div>
                </div>
                <div class="card-container">
                    <div class="card-header"></div>
                    <div class="card-body"></div>
                    <div class="card-footer"></div>
                </div>
                -->
            </div>
            <label class="games-cards-load-more-button" for="load-more-games">
                    <span class="loaded">HIDE</span>
                    <span class="unloaded">SHOW MORE</span>
            </label>
        </div>
        <div class="contacts-wrapper" id="contact">
            <div class="contacts-title">
                <div class="title-container">
                    <h1>CONTACT US</h1>
                </div>
            </div>
            <div class="contacts-content">
                <div class="contacts-description-container">
                    <div class="contact-description-header">
                        <h1>INTERESTED IN WORKING WITH US?</h1>
                    </div>
                    <div class="contact-description-body">
                        <span>Parta Games was founded in 2015 by Antti Kolehmainen and Ville Herranen,
                            after both of them left their respective and respectable day jobs. Since then, Parta Games have developed several titles,
                            including Choppa, their most successful game to this day. In addition, Parta Games does contract work and supports several Unity plugins.
                            </span>
                    </div>
                    <div class="contact-description-footer">
                        <hr>
                        <h2>SOCIAL MEDIA</h2>
                        <div class="social-links-container">
                            <?php get_template_part( 'menu-social', 'social' ); ?>
                        </div>
                    </div>
                </div>
                <div class="contacts-form-container">
                    <div class="contacts-form-header">
                    <h1>LET'S GET TALKING!</h1>
                    </div>

                    <!-- Contact Form -->
                    <?php the_post()?>
                    <div id="post-<?php the_ID()?>" class="post">
                        <div class="entry-content">
                            <form action="<?php the_permalink(); ?>" id='contacts-form' method="POST" class="contacts-form-body">
                                <label class="contacts-form-label" for="contacts-name"> Your Name</label>
                                <input class="contacts-form-input required" id="contacts-name" name="contacts-name"
                                     type="text" value=<?php if(isset($_POST['contacts-name'])) echo $_POST['contacts-name'];?> >
                                
                                <?php if($nameError != '') { ?>
                                    <span class="error"><?=$nameError;?></span>
								<?php } ?>
                                
                                <label class="contacts-form-label" for="contacts-email"> Your Email</label>
                                <input class="contacts-form-input required" id="contacts-email" name="contacts-email"
                                     type="email" value=<?php if(isset($_POST['contacts-email'])) echo $_POST['contacts-email'];?>>
                                
                                <?php if($emailError != '') { ?>
                                    <span class="error"><?=$emailError;?></span>
								<?php } ?>
                                
                                <label class="contacts-form-label" for="contacts-topic"> Subject</label>
                                <input class="contacts-form-input required" id="contacts-topic" name="contacts-topic" 
                                    type="text" value=<?php if(isset($_POST['contacts-topic'])) echo $_POST['contacts-topic'];?>>
                                
                                <?php if($topicError != '') { ?>
                                    <span class="error"><?=$topicError;?></span>
                                <?php } ?>

                                <label class="contacts-form-label" for="contacts-content"> Your Message</label>
                                <textarea rows="10" maxlength="300" class="contacts-form-textarea required" id="contacts-content" name="contacts-content"
                                    type="text" value=<?php if(isset($_POST['contacts-content'])) echo $_POST['contacts-content'];?>></textarea>

                                <?php if($commentError != '') { ?>
                                    <span class="error"><?=$commentError;?></span>
                                <?php } ?>

                                <input id="submit-message" class="contacts-form-submit" type="submit" value="Submit">
                                <input type="hidden" name="contacts-form-submitted" id="contacts-form-submitted" value="true" />
                                
                                
                                <?php if(isset($emailSent) && $emailSent == true) {?>
                                    <div id='contacts-success' class="contacts-success">
                                        <span>Thanks, your email was sent successfully.</span>
                                    </div>
                                    <?php } else {?>
                                        <?php if(isset($hasError)){?>
                                            <div class="contacts-error">Sorry, an error occured.<div>
                                    <?php } ?>
                                <?php } ?>
                            </form>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php get_footer();?>