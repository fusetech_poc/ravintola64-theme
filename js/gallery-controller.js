jQuery(function(){
    console.log('Loaded!')
    jQuery('#selection_prev_button').click(function(e){
        e.preventDefault();
        N2R('#n2-ss-2', function($, slider){slider.previous();});
    });
    jQuery('#selection_next_button').click(function(e){
        e.preventDefault();
        N2R('#n2-ss-2', function($, slider){slider.next();});
    });
    jQuery('#gallery_prev_button').click(function(e){
        e.preventDefault();
        N2R('#n2-ss-3', function($, slider){slider.previous();});
    });
    jQuery('#gallery_next_button').click(function(e){
        e.preventDefault();
        N2R('#n2-ss-3', function($, slider){slider.next();});
    });
})