jQuery(function (){
    
    let $counter = jQuery('<div class="character-counter">'),
    limit=300;

    jQuery('#contacts-content')
        .after($counter)
        .bind('keyup blur', function () {
            let length = jQuery(this).val().length;

            if (length > limit) {
                jQuery(this).val(jQuery(this).val().substring(0, limit))
            } else {
                $counter.text(length + '/' + limit + ' characters');
            }
        })

        .blur()
        
})