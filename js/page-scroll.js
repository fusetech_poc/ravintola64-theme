
jQuery(function(){
  var scrollMagicController = new ScrollMagic.Controller();
  var scene = new ScrollMagic.Scene({
    triggerElement: "#about",
    triggerHook: 0.85,
    duration: 0,
    offset: 0
  })
  .setClassToggle("#scroll-top", "visible")
  //.setPin("#scroll-top")
  .addTo(scrollMagicController);
  
  scrollMagicController.scrollTo(function (newpos) {
    TweenMax.to(window, 1, {scrollTo: {y: newpos}});
  });

	jQuery(document).on("click", "a[href^='#']", function (e) {
		var id = jQuery(this).attr("href");
		if (jQuery(id).length > 0) {
			e.preventDefault();
      console.log('Hi!')
			// trigger scroll
			scrollMagicController.scrollTo(id);

				// if supported by the browser we can even update the URL.
			if (window.history && window.history.pushState) {
				history.pushState("", document.title, id);
			}
    }
  });

})