<?php

add_action( 'init', 'my_register_nav_menus' );

function my_register_nav_menus() {
    register_nav_menu( 'social', __( 'Social', 'partagames-theme' ) );
    register_nav_menu( 'languages', __( 'Languages', 'partagames-theme' ) );

}

function load_stylesheets()
{
    wp_register_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.min.css', array(), false, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style( 'style', get_template_directory_uri().'/style.css', array(), false, 'all');
    wp_enqueue_style('style');
    wp_enqueue_style( 'partagames-fontawesome', 'https://use.fontawesome.com/releases/v5.12.1/css/all.css' );
}
add_action('wp_enqueue_scripts', 'load_stylesheets');


function loadjs()
{
    wp_register_script('gsap', get_template_directory_uri().'/js/gsap.min.js', array ( 'jquery' ), 1, true);
    wp_register_script('scroll-magic', get_template_directory_uri().'/js/ScrollMagic.min.js', array ( 'jquery', 'gsap'), 1, true);
    wp_register_script('animation-gsap', get_template_directory_uri().'/js/plugins/animation.gsap.min.js', array ( 'jquery', 'gsap', 'scroll-magic' ), 1, true);
    wp_register_script('scroll-to', get_template_directory_uri().'/js/plugins/ScrollToPlugin.min.js', array ( 'jquery', 'gsap', 'animation-gsap'), 1, true);
    wp_register_script('page-scroll', get_template_directory_uri().'/js/page-scroll.js', array ( 'jquery', 'gsap', 'scroll-magic', 'scroll-to', 'animation-gsap' ), 1, true);
    wp_register_script('gallery-controller', get_template_directory_uri().'/js/gallery-controller.js', array ('jquery'), 1, true);
    wp_register_script('page-navigation', get_template_directory_uri().'/js/page-navigation.js', array ('jquery'), 1, true);
    wp_register_script('character-counter', get_template_directory_uri().'/js/character-counter.js', array ('jquery'), 1, true);

    wp_register_script('jquery-validation', get_template_directory_uri().'/js/plugins/jquery.validate.min.js', array ('jquery'), 1, true);
    wp_register_script('validation', get_template_directory_uri().'/js/plugins/validation.js', array ('jquery', 'jquery-validation'), 1, true);

    wp_enqueue_script('gsap');
    wp_enqueue_script('scroll-magic');
    wp_enqueue_script('animation-gsap');
    wp_enqueue_script('scroll-to');
    wp_enqueue_script('gallery-controller');
    wp_enqueue_script('page-scroll');
    wp_enqueue_script('character-counter');
    wp_enqueue_script('jquery-validation');
    wp_enqueue_script('validation');

}
add_action('wp_enqueue_scripts', 'loadjs');
add_theme_support( 'title-tag' );
