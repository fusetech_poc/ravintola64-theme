<?php

$nameError = null;
$emailError = null;
$topicError = null;
$commentError = null;



if(isset($_POST['contacts-form-submitted'])) {
	if(trim($_POST['contacts-name']) === '') {
		$nameError = 'Please enter your name.';
		$hasError = true;
	} else {
		$name = trim($_POST['contacts-name']);
	}

	if(trim($_POST['contacts-email']) === '')  {
		$emailError = 'Please enter your email address.';
		$hasError = true;
	} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['contacts-email']))) {
		$emailError = 'You entered an invalid email address.';
		$hasError = true;
	} else {
		$email = trim($_POST['contacts-email']);
	}

    if(trim($_POST['contacts-topic']) === '') {
        $topicError = 'Please specify the topic.';
        $hasError = true;
    } else {
        $topic = trim($_POST['contacts-topic']);
    }

	if(trim($_POST['contacts-content']) === '') {
		$commentError = 'Please enter a message.';
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['contacts-content']));
		} else {
			$comments = trim($_POST['comments']);
		}
	}


    
	if(!isset($hasError)) {
		$emailTo = get_option('tz_email');
		if (!isset($emailTo) || ($emailTo == '') ){
			$emailTo = get_option('admin_email');
		}
		$subject = $topic. ' From '.$name;
		$body = "Name: $name \n\nEmail: $email \n\nComments: $comments";
		$headers = 'From: '.$name.' <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

		wp_mail($emailTo, $subject, $body, $headers);
        $emailSent = true;
        $_POST = array();
    }

} ?>