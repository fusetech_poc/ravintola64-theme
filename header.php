<!DOCTYPE html>
<html>
    <head>
        <?php wp_head();?>
    
    </head>
<body  <?php body_class();?>>

<header>
    
    <div class="theme-header">
        <div class="theme-logo"><span>PARTA GAMES</span></div>
        <div class="nav-container">
            <div class="nav-buttons"><a href="#about">ABOUT</a></div>
            <div class="nav-buttons"><a href="#games">GAMES</a></div>
            <div class="nav-buttons"><a href="#contact">CONTACT</a></div>
        </div>
    </div>
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri();?>/assets/images/favicons/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri();?>/assets/images/favicons/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/faviconapple-touch-icon.png">

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">


</header>
